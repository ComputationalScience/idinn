What Makes Neural Network Controllers Dynamics-Informed?
========================================================

.. figure:: ../_static/optimization_schematic.png
   :align: center

The key underlying principle of the dynamics-informed approach is to incorporate costs calculated by sourcing models into the neural networks.